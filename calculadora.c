#include<stdio.h>
#include<stdlib.h>
#define MAX 20
/*
(A + B + C)		=		ABC++;

(A * (B + C)/D - E)) 		=		ABC+*D/E-
E-A/D*B+C
EADBC-/*+
*/

int pilhaVazia(int posicao){
	getchar();
	if(posicao == -1)
		return 0;
	else
		return -1;
}
int pilhaCheia(int posicao){
	if(posicao == (MAX - 1))
		return 0;
	else
		return -1;
}
int empilha(char c[],int posicao){
	int numero;
	if(pilhaCheia(posicao) == 0)
		return -1;
	else
		return 0;
}
int desempilha(int posicao){
	if(pilhaVazia(posicao) == 0)
		return -1;
	posicao--;
	return 0;
}
void troca(int i, char info[]){
	char aux;
	aux = info[i];
	info[i] = info[i+1];
	info[i+1] = aux;
}
void mostraNotacaoPosFixa(int posicao, char info[]){
	int i = 0, j = 0;
	i = 0;
	while(j <= posicao){
		do{

			if(info[i] == '*' || info[i] == '/'){
				if(i+1 <= posicao && info[i+1] != '+' && info[i+1] != '-' ){
					troca(i,info);
				}
			}
			if(info[i] == '+' || info[i] == '-'){
				if(i+1 <= posicao){
					troca(i,info);
				}
			}
			i++;
		}while(i <= posicao && i < MAX);
		i = 0;
		j++;
	}
	i = 0;
	do{
		if(i == posicao)
			printf("%c\n",info[i]);
		else
			printf("%c",info[i]);
		i++;
	}while(i <= posicao);
}


int main(){
	system("color 4f");
	int posicao = 0, i = 0;
	char info[MAX];
	printf("Informe a expressao numerica: ");
	do{
		info[posicao] = getchar();
		if(info[posicao] != ' ' && info[posicao] != 10)
			posicao++;
	}while(info[posicao] != 10 && posicao < MAX);
	if(info[posicao] == 10)
		posicao--;
	mostraNotacaoPosFixa(posicao, info);
	getchar();
	return 0;
}
